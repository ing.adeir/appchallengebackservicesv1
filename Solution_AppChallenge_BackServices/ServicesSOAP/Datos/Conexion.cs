﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace ServicesSOAP.Datos
{
    public class Conexion
    {
        public String GetConexion
        {
            get
            {
                String _value = ConfigurationManager.ConnectionStrings["BdDist"].ToString();
                return _value;
            }
        }

    }
}