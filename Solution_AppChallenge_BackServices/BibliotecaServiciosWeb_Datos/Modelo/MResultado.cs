﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotecaServiciosWeb_Datos.Modelo
{
    public class MResultado
    {
        public Boolean bResultado { get; set; }
        public string sMensaje { get; set; }

    }
}
