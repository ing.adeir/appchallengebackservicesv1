﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServicesSOAP.Modelo
{
    public class MResultado
    {
        public Boolean bResultado { get; set; }
        public string sMensaje { get; set; }
    }
}