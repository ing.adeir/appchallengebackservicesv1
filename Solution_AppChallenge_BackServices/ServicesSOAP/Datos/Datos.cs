﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServicesSOAP.Modelo;
using System.Data.SqlClient;

namespace ServicesSOAP.Datos
{
    public class ClaseDatos
    {
        Conexion clsConex = new Conexion();

        public List<MEvaluaciones> ListarEvaluaciones(DateTime dFechaRegistroInicio, DateTime dFechaRegistroFin)
        {
            string conx = clsConex.GetConexion;
            List<MEvaluaciones> lista = new List<MEvaluaciones>();
            MEvaluaciones _MEvaluaciones = new MEvaluaciones();

            using (SqlConnection conexion = new SqlConnection(conx))
            {
                using (SqlCommand comando = new SqlCommand("SP_LISTAR_EVALUACIONES", conexion))
                {
                    comando.CommandType = System.Data.CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@dFechaRegistroInicio", dFechaRegistroInicio);
                    comando.Parameters.AddWithValue("@dFechaRegistroFin", dFechaRegistroFin);
                    conexion.Open();
                    SqlDataReader reader = comando.ExecuteReader();
                    while (reader.Read())
                    {
                        _MEvaluaciones = new MEvaluaciones();
                        _MEvaluaciones.lIdEvaluacionServicio = Convert.ToInt64(reader["bIdEvaluacionServicio"].ToString().Trim());
                        _MEvaluaciones.sNombres = reader["vNombres"].ToString().Trim();
                        _MEvaluaciones.sCorreo = reader["vCorreo"].ToString().Trim();
                        _MEvaluaciones.iCalificacion = Convert.ToInt32(reader["iCalificacion"].ToString().Trim());
                        _MEvaluaciones.dFechaRegistro = Convert.ToDateTime(reader["dFechaRegistro"].ToString().Trim());

                        lista.Add(_MEvaluaciones);
                    }
                    conexion.Close();
                    comando.Dispose();
                }
            }

            return lista;
        }


    }
}