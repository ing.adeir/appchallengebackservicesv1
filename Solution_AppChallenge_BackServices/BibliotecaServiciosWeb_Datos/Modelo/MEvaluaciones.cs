﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotecaServiciosWeb_Datos.Modelo
{
    public class MEvaluaciones
    {
        public long lIdEvaluacionServicio {get;set;}
        public string sNombres {get;set;}
        public string sCorreo {get;set;}
        public int iCalificacion {get;set;}
        public DateTime dFechaRegistro { get; set; }
 
    }
}
