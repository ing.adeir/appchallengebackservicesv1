﻿using BibliotecaServiciosWeb_Datos.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BibliotecaServiciosWeb_Datos.Negocio;

namespace AppChallenge_BackServicesV1.Controllers
{
    public class EvaluacionesController : ApiController
    {
       /// <summary>
       ///  Listado de Evaluaiones sin parametros GET
       /// </summary>
       /// <returns></returns>

        // GET api/Evaluaciones --->Consultar Todos
        [HttpGet]
        public IEnumerable<MEvaluaciones> Get()
        {
            NegEvaluaciones _NegEvaluaciones = new NegEvaluaciones();
            var listado = _NegEvaluaciones.ListarEvaluaciones();
            return listado;
        }

        // GET api/Evaluaciones/5 -->Consultar por Id
        [HttpGet]
        public MEvaluaciones Get( int id )
        {
            NegEvaluaciones _NegEvaluaciones = new NegEvaluaciones();
            var Evaluacion = _NegEvaluaciones.ListarEvaluaciones().FirstOrDefault(x => x.lIdEvaluacionServicio == id);
            return Evaluacion;
        }

        // POST api/Evaluaciones ->Insercion
        public MResultado Post( [FromBody] MEvaluaciones evaluacion)
        {
            NegEvaluaciones _NegEvaluaciones = new NegEvaluaciones();
            var MResultado = _NegEvaluaciones.InsertarEvaluacion(evaluacion);
            return MResultado;
        }

        // PUT api/Evaluaciones/5 -->Actualizacion
        public MResultado Put([FromBody] MEvaluaciones evaluacion)
        {
            NegEvaluaciones _NegEvaluaciones = new NegEvaluaciones();
            var MResultado = _NegEvaluaciones.ActualizarEvaluacion(evaluacion);
            return MResultado;
        }


        // DELETE api/Evaluaciones/5 --> Eliminacion
        public MResultado Delete(int id)
        {
            NegEvaluaciones _NegEvaluaciones = new NegEvaluaciones();
            MEvaluaciones evaluacion = new MEvaluaciones();
            evaluacion.lIdEvaluacionServicio = id;
            evaluacion.sCorreo = "";
            evaluacion.sNombres = "";
            var MResultado = _NegEvaluaciones.EliminarEvaluacion(evaluacion);
            return MResultado;
        }


    }
}
