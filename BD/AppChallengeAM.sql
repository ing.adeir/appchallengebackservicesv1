

create database AppChallengeAM;


CREATE TABLE TB_EvaluacionServicios(
	bIdEvaluacionServicio bigint identity(1,1) primary key,
	vNombres varchar(100),
	vCorreo varchar(100),
	iCalificacion int,
	dFechaRegistro datetime
);


----STORE PROCEDURES

alter PROC SP_CRUD_EVALUACIONES
@vValor VARCHAR(5),
@bIdEvaluacionServicio	BIGINT,
@vNombres VARCHAR(100),
@vCorreo	 VARCHAR(100),
@iCalificacion INT
AS
BEGIN

	
	IF(@vValor = 'C')
	BEGIN
	
		INSERT INTO TB_EvaluacionServicios
		(			
			vNombres,
			vCorreo,
			iCalificacion,
			dFechaRegistro
		)
		SELECT 
			@vNombres,
			@vCorreo,
			@iCalificacion,
			GETDATE()
		
	END
	


	IF(@vValor = 'R')
	BEGIN
		
		SELECT 
			bIdEvaluacionServicio,
			vNombres,
			vCorreo,
			iCalificacion,
			dFechaRegistro
		 FROM TB_EvaluacionServicios
		 ORDER BY dFechaRegistro DESC
		
	END
		

	IF(@vValor = 'U')
	BEGIN
	
		UPDATE TB_EvaluacionServicios
		SET 		
			vNombres = @vNombres,
			vCorreo = @vCorreo,
			iCalificacion = @iCalificacion
		WHERE bIdEvaluacionServicio = @bIdEvaluacionServicio
		
	END
	
	IF(@vValor = 'D')
	BEGIN
	
		DELETE FROM  TB_EvaluacionServicios
		WHERE bIdEvaluacionServicio = @bIdEvaluacionServicio
		
	END
	
END;



ALTER PROC SP_LISTAR_EVALUACIONES
@dFechaRegistroInicio DATETIME,
@dFechaRegistroFin DATETIME
AS
BEGIN

	
		SELECT bIdEvaluacionServicio,
			vNombres,
			vCorreo,
			iCalificacion,
			dFechaRegistro
		FROM  TB_EvaluacionServicios
		WHERE dFechaRegistro BETWEEN @dFechaRegistroInicio AND @dFechaRegistroFin		
		ORDER BY dFechaRegistro DESC
		
END;



