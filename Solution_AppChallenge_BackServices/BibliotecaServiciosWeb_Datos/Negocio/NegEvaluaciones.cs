﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BibliotecaServiciosWeb_Datos.Datos;
using BibliotecaServiciosWeb_Datos.Modelo;

namespace BibliotecaServiciosWeb_Datos.Negocio
{
    public class NegEvaluaciones
    {
        clsDatos _Datos = new clsDatos();

        public List<MEvaluaciones> ListarEvaluaciones()
        {
            _Datos = new clsDatos();
            return _Datos.ListarEvaluaciones();
        }

        public MResultado InsertarEvaluacion(MEvaluaciones _MEvaluaciones)
        {
            _Datos = new clsDatos();
            return _Datos.InsertarEvaluacion(_MEvaluaciones);
        }
        public MResultado ActualizarEvaluacion(MEvaluaciones _MEvaluaciones)
        {
            _Datos = new clsDatos();
            return _Datos.ActualizarEvaluacion(_MEvaluaciones);
        }

        public MResultado EliminarEvaluacion(MEvaluaciones _MEvaluaciones)
        {
            _Datos = new clsDatos();
            return _Datos.EliminarEvaluacion(_MEvaluaciones);
        }


    }
}
