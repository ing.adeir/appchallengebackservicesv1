﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using BibliotecaServiciosWeb_Datos.Modelo;

namespace BibliotecaServiciosWeb_Datos.Datos
{
    public class clsDatos
    {
        Conexion clsConex = new Conexion();

        public List<MEvaluaciones> ListarEvaluaciones()
        {
            string conx = clsConex.GetConexion;
            List<MEvaluaciones> lista = new List<MEvaluaciones>();
            MEvaluaciones _MEvaluaciones = new MEvaluaciones();

            using (SqlConnection conexion = new SqlConnection(conx))
            {
                using (SqlCommand comando = new SqlCommand("SP_CRUD_EVALUACIONES", conexion))
                {
                    comando.CommandType = System.Data.CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@vValor", "R");
                    comando.Parameters.AddWithValue("@bIdEvaluacionServicio", "");
                    comando.Parameters.AddWithValue("@vNombres", "");
                    comando.Parameters.AddWithValue("@vCorreo", "");
                    comando.Parameters.AddWithValue("@iCalificacion", "");
                    conexion.Open();
                    SqlDataReader reader = comando.ExecuteReader();
                    while (reader.Read())
                    {
                        _MEvaluaciones = new MEvaluaciones();
                        _MEvaluaciones.lIdEvaluacionServicio = Convert.ToInt64(reader["bIdEvaluacionServicio"].ToString().Trim());
                        _MEvaluaciones.sNombres = reader["vNombres"].ToString().Trim();
                        _MEvaluaciones.sCorreo = reader["vCorreo"].ToString().Trim();
                        _MEvaluaciones.iCalificacion = Convert.ToInt32(reader["iCalificacion"].ToString().Trim());
                        _MEvaluaciones.dFechaRegistro = Convert.ToDateTime(reader["dFechaRegistro"].ToString().Trim());

                        lista.Add(_MEvaluaciones);
                    }
                    conexion.Close();
                    comando.Dispose();
                }
            }

            return lista;
        }

        public MResultado InsertarEvaluacion(MEvaluaciones _MEvaluaciones)
        {
            string conx = clsConex.GetConexion;
            MResultado _MResultado = new MResultado();
            try
            {
                using (SqlConnection conexion = new SqlConnection(conx))
                {
                    using (SqlCommand comando = new SqlCommand("SP_CRUD_EVALUACIONES", conexion))
                    {
                        comando.CommandType = System.Data.CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@vValor", "C");
                        comando.Parameters.AddWithValue("@bIdEvaluacionServicio", _MEvaluaciones.lIdEvaluacionServicio.ToString().Trim());
                        comando.Parameters.AddWithValue("@vNombres", _MEvaluaciones.sNombres.ToString().Trim());
                        comando.Parameters.AddWithValue("@vCorreo", _MEvaluaciones.sCorreo.ToString().Trim());
                        comando.Parameters.AddWithValue("@iCalificacion", _MEvaluaciones.iCalificacion.ToString().Trim());
                        conexion.Open();
                        comando.ExecuteNonQuery();
                        _MResultado.bResultado = true;
                        _MResultado.sMensaje = "La operación se realizó correctamente!";
                        conexion.Close();
                        comando.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                _MResultado.bResultado = false;
                _MResultado.sMensaje = ex.ToString();
            }
            return _MResultado;

        }


        public MResultado ActualizarEvaluacion(MEvaluaciones _MEvaluaciones)
        {
            string conx = clsConex.GetConexion;
            MResultado _MResultado = new MResultado();
            try
            {
                using (SqlConnection conexion = new SqlConnection(conx))
                {
                    using (SqlCommand comando = new SqlCommand("SP_CRUD_EVALUACIONES", conexion))
                    {
                        comando.CommandType = System.Data.CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@vValor", "U");
                        comando.Parameters.AddWithValue("@bIdEvaluacionServicio", _MEvaluaciones.lIdEvaluacionServicio.ToString().Trim());
                        comando.Parameters.AddWithValue("@vNombres", _MEvaluaciones.sNombres.ToString().Trim());
                        comando.Parameters.AddWithValue("@vCorreo", _MEvaluaciones.sCorreo.ToString().Trim());
                        comando.Parameters.AddWithValue("@iCalificacion", _MEvaluaciones.iCalificacion.ToString().Trim());
                        conexion.Open();
                        comando.ExecuteNonQuery();
                        _MResultado.bResultado = true;
                        _MResultado.sMensaje = "La operación se realizó correctamente!";
                        conexion.Close();
                        comando.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                _MResultado.bResultado = false;
                _MResultado.sMensaje = ex.ToString();
            }
            return _MResultado;

        }


        public MResultado EliminarEvaluacion(MEvaluaciones _MEvaluaciones)
        {
            string conx = clsConex.GetConexion;
            MResultado _MResultado = new MResultado();
            try
            {
                using (SqlConnection conexion = new SqlConnection(conx))
                {
                    using (SqlCommand comando = new SqlCommand("SP_CRUD_EVALUACIONES", conexion))
                    {
                        comando.CommandType = System.Data.CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@vValor", "D");
                        comando.Parameters.AddWithValue("@bIdEvaluacionServicio", _MEvaluaciones.lIdEvaluacionServicio.ToString().Trim());
                        comando.Parameters.AddWithValue("@vNombres", _MEvaluaciones.sNombres.ToString().Trim());
                        comando.Parameters.AddWithValue("@vCorreo", _MEvaluaciones.sCorreo.ToString().Trim());
                        comando.Parameters.AddWithValue("@iCalificacion", _MEvaluaciones.iCalificacion.ToString().Trim());
                        conexion.Open();
                        comando.ExecuteNonQuery();
                        _MResultado.bResultado = true;
                        _MResultado.sMensaje = "La operación se realizó correctamente!";
                        conexion.Close();
                        comando.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                _MResultado.bResultado = false;
                _MResultado.sMensaje = ex.ToString();
            }
            return _MResultado;

        }



    }
}
