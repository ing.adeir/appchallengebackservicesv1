﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServicesSOAP.Datos;
using ServicesSOAP.Modelo;

namespace ServicesSOAP.Negocio
{
    public class NegEvaluaciones
    {
        ClaseDatos _Datos = new ClaseDatos();

        public List<MEvaluaciones> ListarEvaluaciones(DateTime dFechaRegistroInicio, DateTime dFechaRegistroFin)
        {
            _Datos = new ClaseDatos();
            return _Datos.ListarEvaluaciones(dFechaRegistroInicio, dFechaRegistroFin);
        }

    }
}